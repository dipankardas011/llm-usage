import image_gen
from dotenv import load_dotenv

load_dotenv()
if __name__ == "__main__":
    usr_inp = input("> Enter your prompt for the image generation\n> ")

    img_urls = image_gen.genImage(prompt=usr_inp, isHD=True)
    for img_url in img_urls:
        print(f"Image Url: {img_url}")
