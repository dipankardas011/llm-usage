
def genImage(prompt: str, no_of_img: int = 1, isHD: bool = False) -> list[str | None]:
    from openai import OpenAI
    client = OpenAI()

    quality = "standard"
    if isHD:
        quality = "hd"
    response = client.images.generate(
        prompt=prompt,
        n=no_of_img,
        quality=quality,
        model="dall-e-3",
        size="1024x1024"
    )

    return [x.url for x in response.data]


