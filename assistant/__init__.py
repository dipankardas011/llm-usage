
from openai.types.beta.threads.text_content_block import TextContentBlock
from openai.types.beta.threads.image_file_content_block import ImageFileContentBlock
from openai.types.beta.threads.image_url_content_block import ImageURLContentBlock


def useAssistant() -> None:
    from openai import OpenAI
    client = OpenAI()

    assistant = client.beta.assistants.create(
      name="Math Tutor",
      instructions="You are a personal math tutor. Write and run code to answer math questions.",
      tools=[{"type": "code_interpreter"}],
      model="gpt-4o",
    )
    thread = client.beta.threads.create()
    _ = client.beta.threads.messages.create(
      thread_id=thread.id,
      role="user",
      content="I need to solve the equation `3x + 11 = 14`. Can you help me?"
    )

    run = client.beta.threads.runs.create_and_poll(
      thread_id=thread.id,
      assistant_id=assistant.id,
      instructions="Please address the user as Jane Doe. The user has a premium account."
    )

    if run.status == 'completed': 
      messages = client.beta.threads.messages.list(
        thread_id=thread.id
      )
      data = messages.data

      for message in data:
        raw_data = message.content[0]
        print("="*10 +"Content Block" + "="*10)

        if isinstance(raw_data, TextContentBlock):
          print(raw_data.type)
          print("Value")
          print(raw_data.text.value)

        elif isinstance(raw_data, ImageFileContentBlock):
          print(raw_data.type)
          print(f"file_id: {raw_data.image_file.file_id}")
          print(raw_data.image_file.detail)
        
        else:
          print(raw_data.type)
          print(f"image_url: {raw_data.image_url.url}")
          print(raw_data.image_url.detail)

        print("="*10 +"Content Block" + "="*10)
    else:
      print(run.status)

